import {
  addMr,
  addNameToComment,
  changeSelectedMr,
  goToFeedbackForm,
  isCommentValid,
  isNameValid,
  isEmailValid,
  isPatValid,
  postComment,
  saveComment,
  saveName,
  savePersonalAccessToken,
  toggleExpansion,
} from '../components/helpers';

import {
  ADD_NAME_BUTTON,
  ADD_NAME_CANCEL_BUTTON,
  ADD_PAT_BUTTON,
  ADD_PAT_CANCEL_BUTTON,
  CHANGE_MR_ID_BUTTON,
  COMMENT_BUTTON,
  EXPANSION_BUTTON,
  EXPANSION_BUTTON_CONTAINER,
  MR_ID_BUTTON,
  MR_ID_CANCEL_BUTTON,
} from '../shared';

import { state } from './state';
import debounce from './utils';

const noop = () => {};

// State needs to be bound here to be acted on
// because these are called by click events and
// as such are called with only the `event` object
const eventLookup = id => {
  switch (id) {
    case ADD_NAME_BUTTON:
      saveName(state);
      if (!isNameValid(state) || !isEmailValid(state)) {
        return noop;
      }
      return () => {
        goToFeedbackForm(state);
        postComment(state);
      };
    case ADD_NAME_CANCEL_BUTTON:
      return () => {
        saveName(state);
        goToFeedbackForm(state);
      };
    case ADD_PAT_BUTTON:
      savePersonalAccessToken(state);
      if (!isPatValid(state)) {
        return noop;
      }
      return () => {
        goToFeedbackForm(state);
        postComment(state);
      };
    case ADD_PAT_CANCEL_BUTTON:
      return () => {
        savePersonalAccessToken(state);
        goToFeedbackForm(state);
      };
    case CHANGE_MR_ID_BUTTON:
      return () => {
        saveComment(state);
        changeSelectedMr(state);
      };
    case COMMENT_BUTTON:
      saveComment(state);
      if (!isCommentValid(state)) {
        return noop;
      }
      return addNameToComment.bind(null, state);
    case EXPANSION_BUTTON:
      return toggleExpansion.bind(null, state);
    case EXPANSION_BUTTON_CONTAINER:
      return toggleExpansion.bind(null, state);
    case MR_ID_CANCEL_BUTTON:
      return toggleExpansion.bind(null, state);
    case MR_ID_BUTTON:
      return addMr.bind(null, state);
    default:
      return noop;
  }
};

const updateWindowSize = wind => {
  state.innerWidth = wind.innerWidth;
  state.innerHeight = wind.innerHeight;
};

const initializeGlobalListeners = () => {
  window.addEventListener('resize', debounce(updateWindowSize.bind(null, window), 200));
  window.addEventListener('beforeunload', event => {
    if (state.usingGracefulStorage) {
      // if there is no browser storage support, reloading will lose the comment; this way, the user will be warned
      // we assign the return value because it is required by Chrome see: https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload#Example,
      event.preventDefault();
      /* eslint-disable-next-line no-param-reassign */
      event.returnValue = '';
    }

    saveComment();
  });
};

export { eventLookup, initializeGlobalListeners };
