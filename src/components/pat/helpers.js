import { clearNote, postError } from '../note/helpers';
import { ADD_PAT } from '../../shared';

export { getPersonalAccessToken, savePersonalAccessToken } from './storage';

export const isPatValid = ({ personalAccessToken }) => {
  if (!personalAccessToken) {
    postError('Your personal access token appears to be empty.', ADD_PAT);
    return false;
  }
  clearNote();
  return true;
};
