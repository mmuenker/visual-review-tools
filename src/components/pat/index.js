import { ADD_PAT, PAT_FIELD_CONTAINER } from '../../shared';
import { getPersonalAccessToken } from './storage';

const patLabel = 'Personal Access Token';
const patDocsUrl =
  'https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token';
const patDocsLink = `<a target="_blank" rel="noopener noreferrer" class="gl-blue-600 gl-no-underline" href="${patDocsUrl}">personal access token</a>`;

const patField = state =>
  `
    <div id="${PAT_FIELD_CONTAINER}">
      <label for="${ADD_PAT}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${patLabel}</label>
      <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-2 gl-min-h-5 gl-text-normal gl-box-sizing-border-box" type="password" id="${ADD_PAT}" name="${ADD_PAT}" aria-required="true" value="${getPersonalAccessToken(
    state
  )}">
      <div class="gl-gray-700 gl-my-2 gl-text-light">
        You must provide a ${patDocsLink} that is granted the "api" scope.
      </div>
    </div>
  `;

export default patField;
