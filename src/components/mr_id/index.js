import { MR_ID, MR_ID_CANCEL_BUTTON, MR_ID_BUTTON } from '../../shared';
import { rememberBox } from '../form_elements';
import { buttonClearStyles } from '../utils';

const mrRememberText = 'Remember ID';
const visualReviewDocumentationLinkStart =
  '<a href="https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter" target="_blank" rel="noopener noreferrer" class="gl-blue-600 gl-no-underline">';
const visualReviewDocumentationLinkEnd = '</a>';
const mrLabel = 'Enter your merge request ID';
const description = `A ${visualReviewDocumentationLinkStart}merge request ID${visualReviewDocumentationLinkEnd} was not set for this review app. To provide feedback, enter an ID for a merge request relevant to your review.`;

const mrForm = `
    <div>
      <label for="${MR_ID}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${mrLabel}</label>
      <div class="gl-mb-6">${description}</div>
      <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-2 gl-min-h-5 gl-text-normal gl-box-sizing-border-box" type="text" pattern="[1-9][0-9]*" id="${MR_ID}" name="${MR_ID}" placeholder="e.g: 7745" aria-required="true">
    </div>
    ${rememberBox(mrRememberText)}
    <div class="gl-flex gl-justify-flex-end gl-mt-2">
      <button class="gitlab-button gitlab-button-secondary" style="${buttonClearStyles}" type="button" id="${MR_ID_CANCEL_BUTTON}">Cancel</button>
      <button class="gitlab-button gitlab-button-success" style="${buttonClearStyles}" type="button" id="${MR_ID_BUTTON}">Submit</button>
    </div>
`;

export default mrForm;
