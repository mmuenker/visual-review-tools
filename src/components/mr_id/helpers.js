import { clearNote, postError } from '../note/helpers';
import { MR_ID, STORAGE_MR_ID, localStorage } from '../../shared';
import { nextView } from '../../store/view';
import { selectForm, selectMrBox, selectRemember } from '../utils';
import { addForm } from '../wrapper/helpers';

const storeMr = (id, state) => {
  const rememberMe = selectRemember().checked;

  if (rememberMe) {
    localStorage.setItem(STORAGE_MR_ID, id);
  }

  state.mergeRequestId = id;
};

const getFormError = (mrNumber, form) => {
  if (!mrNumber) {
    return 'Please enter your merge request ID number.';
  }

  if (!form.checkValidity()) {
    return 'Please remove any non-number values from the field.';
  }

  return null;
};

const addMr = state => {
  // Clear any old errors
  clearNote(MR_ID);

  const mrNumber = selectMrBox().value;
  const form = selectForm();
  const formError = getFormError(mrNumber, form);

  if (formError) {
    postError(formError, MR_ID);
    return;
  }

  storeMr(mrNumber, state);
  addForm(nextView(state, MR_ID));
};

export { addMr, storeMr };
