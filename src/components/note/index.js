import { NOTE, NOTE_CONTAINER } from '../../shared';

const note = `
  <div id="${NOTE_CONTAINER}" class="gl-flex gl-flex-column gl-shadow gl-border-r-1 gl-bkg-white gl-mb-2 gl-p-3 gl-max-w-40 gl-box-sizing-border-box gl-ml-6 gl-visibility-hidden">
    <div id="${NOTE}" class="gl-mb-0"></div>
  </div>
`;

export default note;
