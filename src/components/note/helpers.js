import { selectById, selectNote, selectNoteContainer } from '../utils';

export const clearNote = inputId => {
  const currentNote = selectNote();
  const noteContainer = selectNoteContainer();

  noteContainer.classList.remove('gl-visibility-visible');
  noteContainer.classList.add('gl-visibility-hidden');
  currentNote.innerText = '';
  currentNote.classList.remove('gl-white');
  noteContainer.classList.remove('gl-bkg-red-500');
  noteContainer.classList.add('gl-bkg-white');

  if (inputId) {
    const field = document.getElementById(inputId);
    field.classList.remove('gl-border-red-500');
    field.classList.add('gl-border-gray-200');
  }
};

export const postInfo = message => {
  const currentNote = selectNote();
  const noteContainer = selectNoteContainer();
  currentNote.insertAdjacentHTML('beforeend', message);
  noteContainer.classList.remove('gl-visibility-hidden');
  noteContainer.classList.add('gl-visibility-visible');
  setTimeout(clearNote, 5000);
};

export const postError = (message, inputId) => {
  const currentNote = selectNote();
  const noteContainer = selectNoteContainer();
  const field = selectById(inputId);
  field.classList.remove('gl-border-gray-200');
  field.classList.add('gl-border-red-500');
  currentNote.classList.add('gl-white');
  currentNote.innerText = message;
  noteContainer.classList.remove('gl-visibility-hidden');
  noteContainer.classList.add('gl-visibility-visible');
  noteContainer.classList.remove('gl-bkg-white');
  noteContainer.classList.add('gl-bkg-red-500');
  setTimeout(clearNote.bind(null, inputId), 5000);
};
