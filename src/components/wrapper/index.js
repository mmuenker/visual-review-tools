import {
  EXPANSION_BUTTON,
  EXPANSION_BUTTON_CONTAINER,
  EXPANSION_CONTAINER,
  FORM,
  FORM_CONTAINER,
} from '../../shared';
import { expandRightIcon, logo } from './icons';

const form = content => `
  <form id="${FORM}" class="gl-flex gl-flex-column gl-w-100p gl-mb-0" novalidate>
    ${content}
  </form>
`;

const buttonAndForm = content => `
<div id="${EXPANSION_CONTAINER}" class="gl-inline-flex gl-align-center">
  <div class="gl-flex gl-flex-column gl-align-center gl-py-1 gl-bkg-gray-50 gl-border-rl-1 gl-shadow">
    <div class="gl-w-35 gl-px-2 gl-mt-1">
      ${logo}
    </div>
    <div class="gl-text-normal gl-writing-mode-sideways-lr gl-my-2 gl-mr-05 gl-gray-700">
      Review
    </div>
    <div id="${EXPANSION_BUTTON_CONTAINER}" class="gl-pointer gl-flex gl-justify-center gl-w-100p gl-border-top-1 gl-border-gray-200 gl-py-1 gl-gray-700" >
      ${expandRightIcon(EXPANSION_BUTTON)}
    </div>
  </div>
  <div id="${FORM_CONTAINER}" class="gl-p-3 gl-bkg-white gl-shadow gl-border-rl-2 gl-overflow-auto gl-content-box gl-box-sizing-border-box">
    ${form(content)}
  </div>
</div>
`;

export default buttonAndForm;
