import { CHANGE_MR_ID_BUTTON, REMEMBER_ITEM } from '../shared';
import { buttonClearStyles } from './utils';

export const feedbackHeader = mergeRequestId => `
  <div class="gl-flex gl-align-center gl-mb-2">
    <div class="gl-text-heavy gl-max-w-30 gl-text-overflow-ellipsis gl-overflow-hidden gl-white-space-nowrap">
      Review for merge request !${mergeRequestId}
    </div>
    <div class="gl-ml-auto gl-mr-3 gl-border-w-1 gl-border-left-solid gl-border-left-gray-200 gl-pl-2 gl-line-height-normal">
      <button id="${CHANGE_MR_ID_BUTTON}" class="gl-blue-600 gl-no-underline gl-underline-hover gitlab-link-button gl-text-heavy">Change MR</button>
    </div>
  </div>
`;

export const rememberBox = (rememberText = 'Remember me') => `
  <div class="gl-flex gl-align-center">
    <input type="checkbox" id="${REMEMBER_ITEM}" class="gl-ml-0" name="${REMEMBER_ITEM}" checked />
    <label for="${REMEMBER_ITEM}" class="gl-p-1">${rememberText}</label>
  </div>
`;

export const submitButton = buttonId => `
  <div class="js-gitlab-button-wrapper gl-flex gl-align-baseline gl-mt-2">
    <button class="gitlab-button gitlab-button-success gl-w-100p" style="${buttonClearStyles}" type="submit" id="${buttonId}"> Submit </button>
  </div>
`;
