import { sessionStorage, STORAGE_NAME, STORAGE_EMAIL } from '../../shared';
import { selectAddNameBox, selectAddEmailBox } from '../utils';

export const getSavedName = (state = {}) =>
  sessionStorage.getItem(STORAGE_NAME) || state.userName || '';
export const getSavedEmail = (state = {}) =>
  sessionStorage.getItem(STORAGE_EMAIL) || state.userEmail || '';

export const saveName = state => {
  const nameBox = selectAddNameBox();
  const emailBox = selectAddEmailBox();

  if (nameBox && nameBox.value) {
    const userName = nameBox.value.trim();
    sessionStorage.setItem(STORAGE_NAME, userName);
    state.userName = userName;
  }

  if (emailBox && emailBox.value) {
    const userEmail = emailBox.value.trim();
    sessionStorage.setItem(STORAGE_EMAIL, userEmail);
    state.userEmail = userEmail;
  }
};
