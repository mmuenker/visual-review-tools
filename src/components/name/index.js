import { ADD_NAME, ADD_EMAIL, NAME_FIELDS_CONTAINER } from '../../shared';
import { getSavedName, getSavedEmail } from './storage';

const addNameLabel = 'Reviewer name';
const addEmailLabel = 'Reviewer email (optional)';

const nameFields = state => {
  const name = getSavedName(state);
  const email = getSavedEmail(state);
  return `
    <div id="${NAME_FIELDS_CONTAINER}">
      <div>
        <label for="${ADD_NAME}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${addNameLabel}</label>
        <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-2 gl-min-h-5 gl-text-normal gl-box-sizing-border-box" type="text" id="${ADD_NAME}" name="${ADD_NAME}" placeholder="John Doe or Anonymous" aria-required="true" value="${name}">
      </div>
      <div class="gl-mt-2">
        <label for="${ADD_EMAIL}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${addEmailLabel}</label>
        <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-2 gl-min-h-5 gl-text-normal gl-box-sizing-border-box" type="text" id="${ADD_EMAIL}" name="${ADD_EMAIL}" placeholder="john.doe@gitlab.com" aria-required="true" value=${email}>
      </div>
    </div>
  `;
};

export default nameFields;
