import { BLACK, COMMENT_BOX, MUTED } from '../../shared';
import { clearSavedComment } from './storage';
import { clearNote, postError, postInfo } from '../note/helpers';
import { selectCommentBox, selectCommentButton } from '../utils';

const resetCommentButton = () => {
  const commentButton = selectCommentButton();

  commentButton.innerText = 'Next';
  commentButton.classList.replace('gitlab-button-secondary', 'gitlab-button-success');
  commentButton.style.opacity = 1;
};

const resetCommentBox = () => {
  const commentBox = selectCommentBox();
  commentBox.style.pointerEvents = 'auto';
  commentBox.style.color = BLACK;
};

const resetCommentText = () => {
  const commentBox = selectCommentBox();
  commentBox.value = '';
  clearSavedComment();
};

const resetComment = () => {
  resetCommentButton();
  resetCommentBox();
  resetCommentText();
};

const confirmAndClear = feedbackInfo => {
  const commentButton = selectCommentButton();

  commentButton.innerText = 'Feedback sent';
  postInfo(feedbackInfo);

  setTimeout(resetComment, 1000);
};

const setInProgressState = () => {
  const commentButton = selectCommentButton();
  const commentBox = selectCommentBox();

  commentButton.innerText = 'Sending feedback';
  commentButton.classList.replace('gitlab-button-success', 'gitlab-button-secondary');
  commentButton.style.opacity = 0.5;
  commentBox.style.color = MUTED;
  commentBox.style.pointerEvents = 'none';
};

const commentErrors = error => {
  switch (error.status) {
    case 401:
      return 'Unauthorized.';
    case 403:
      return 'Anonymous visual review feedback is disabled. Enable the "anonymous_visual_review_feedback" feature flag.';
    case 404:
      return 'Not found. You may have entered an incorrect merge request ID.';
    default:
      return 'Something went wrong on our end. Please try again.';
  }
};

const isCommentValid = ({ comment }) => {
  if (!comment) {
    postError('Your comment appears to be empty.', COMMENT_BOX);
    resetCommentBox();
    resetCommentButton();
    return false;
  }
  return true;
};

const senderText = (userName, userEmail) => {
  let userDetails = userName;

  if (userEmail) {
    userDetails = `${userName} (${userEmail})`;
  }

  return `**Feedback from ${userDetails}**\n\n`;
};

const postComment = ({
  comment,
  platform,
  browser,
  userAgent,
  innerWidth,
  innerHeight,
  personalAccessToken,
  projectId,
  projectPath,
  mergeRequestId,
  mrUrl,
  userName,
  userEmail,
}) => {
  // Clear any old errors
  clearNote(COMMENT_BOX);

  setInProgressState();
  // Get the href at the last moment to support SPAs
  const { href } = window.location;

  let nameText = '';

  if (!personalAccessToken) {
    nameText = senderText(userName, userEmail);
  }

  const detailText = `
    \n
<details>
  <summary>Metadata</summary>
  Posted from ${href} | ${platform} | ${browser} | ${innerWidth} x ${innerHeight}.
  <br /><br />
  <em>User agent: ${userAgent}</em>
</details>
  `;

  const url = `${mrUrl}/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/visual_review_discussions`;

  const body = `${nameText} ${comment} ${detailText}`;
  const linkClasses = 'gl-blue-600 gl-no-underline gl-underline-hover';

  const headers = { 'Content-Type': 'application/json' };

  if (personalAccessToken) {
    headers['Private-Token'] = personalAccessToken;
  }

  return fetch(url, {
    method: 'POST',
    body: JSON.stringify({ body }),
    headers,
  })
    .then(response => {
      if (response.ok) {
        return response.json();
      }

      throw response;
    })
    .then(data => {
      const commentId = data.notes[0].id;
      const feedbackLink = `${mrUrl}/${projectPath}/merge_requests/${mergeRequestId}#note_${commentId}`;
      const feedbackInfo = `Feedback sent. View at <a class="${linkClasses}" href="${feedbackLink}" target="_blank">${projectPath} !${mergeRequestId} (comment ${commentId})</a>`;
      confirmAndClear(feedbackInfo);
    })
    .catch(err => {
      postError(commentErrors(err), COMMENT_BOX);
      resetCommentBox();
      resetCommentButton();
    });
};

export { isCommentValid, postComment };
