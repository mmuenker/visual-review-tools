import { COMMENT_BOX, COMMENT_BUTTON } from '../../shared';
import { buttonClearStyles } from '../utils';
import { feedbackHeader } from '../form_elements';
import { getSavedComment } from './storage';

const comment = state => {
  const savedComment = getSavedComment();

  return `
    <div>
      ${feedbackHeader(state.mergeRequestId)}
      <textarea
        id="${COMMENT_BOX}"
        name="${COMMENT_BOX}"
        rows="3"
        placeholder="e.g: Modal fonts are wrong..."
        class="gl-bkg-white gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-min-h-5 gl-p-1 gl-box-sizing-border-box gl-resize-none gl-text-normal"
        aria-required="true"
      >${savedComment}</textarea>
      <div class="gl-gray-700 gl-my-2 gl-text-light">
        This feedback will be added to merge request !${state.mergeRequestId}.
        Additional metadata about your session will be included.
      </div>
    </div>
    <div class="gl-flex gl-justify-flex-end">
      <button class="gitlab-button gitlab-button-success" style="${buttonClearStyles}" type="button" id="${COMMENT_BUTTON}">Next</button>
    </div>
  `;
};

export default comment;
