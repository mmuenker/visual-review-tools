# Visual Review Tools

This is the repo for visual review tools. Someday this README will have more info. For information on the feature itself, check out [the docs](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter).

## Development

This project is developed in vanilla Javascript only. Because we are asking our users to inject this toolbar into their application for review, it is important to keep things small and avoid dependencies on large scripts or other loads. As we continue to develop, we will revisit these decisions, but for now, plain JS it is!

However, in order to have some structure, we have designed a component-like system with a tiny event reducer and state object.

### Components

Similar to the Vue components in our main repo, Visual Review components are developed in a single file with a template and related events functions. Templates consist of template strings that can be pasted into the DOM using `insertAdjacentHTML`. For instance:

```js
const note = `
  <div id="${NOTE_CONTAINER}" style="visibility: hidden;">
    <p id="${NOTE}" class="gl-mb-0"></p>
  </div>
`;
```

**Because `insertAdjacentHTML` inserts strings as HTML, this is a possible XSS vector. Do not paste unescaped user-provided content into this without escaping.**

Event functions are grouped with the template that calls them. That's why, for instance, you will find `logoutUser` in `comment.js`.

Templates and actions that are referenced outside the `components` directory should be added to `components/index.js` so that users do not have to know the subfile from which to import things. This is important especially in a young app so that restructuring can happed behind the scenes, without consumers needing to know.

The same principle applies with sub-components (e.g. `comment_mr_note`) passing their functions through the main section module. When the file list starts looking complex, this can be reorganized into their own directory.

### Constants

Because these is plain Javascript, we change components by referencing them via ID. This is a spot that's rife with errors, so please use `shared/constants.js` to render the string as a constant and create an access function. (You may wonder why it is a list of functions and not an object with getters/setters. It's because we cannot destructure imports and this way just the necessary accessor can be imported. Also Sarah really likes FP and hates OO.)

### Events

Speaking of FP, we dispatch events using a poor-woman's reducer, found in `store/events.js`. Essentially, we have a single event listener on the form and this delegates to the necessary event using the `event target`. Be sure to make use of our friendly constants!

## Testing Locally

Run `yarn start` in the root directory to start a webpack dev server. This uses a basic `index.html` file to test the javascript library.

By default, the dev server port is set to `8080`. If you would like to change the port of the dev server, run `DEV_SERVER_PORT=<port> yarn start` instead.

Currently, the review app is tied to a test merge request: [https://gitlab.com/gitlab-org/visual-review-tools/-/merge_requests/15](https://gitlab.com/gitlab-org/visual-review-tools/-/merge_requests/15). This can be changed in the `dev/index.html` file.
