import userInfoForm from '~/components/user_info';
import { setHTMLFixture } from '../../helpers';
import {
  ADD_NAME_BUTTON,
  ADD_NAME_CANCEL_BUTTON,
  ADD_PAT_BUTTON,
  ADD_PAT_CANCEL_BUTTON,
  NAME_FIELDS_CONTAINER,
  PAT_FIELD_CONTAINER,
} from '~/shared';

describe('the name input component', () => {
  describe('when requireAuth is falsey or undefined', () => {
    beforeEach(() => {
      setHTMLFixture(userInfoForm({}));
    });

    it('contains the name fields component', () => {
      expect(document.getElementById(NAME_FIELDS_CONTAINER)).toBeInTheDocument();
    });

    it('contains the submit button for adding name', () => {
      expect(document.getElementById(ADD_NAME_BUTTON)).toBeInTheDocument();
    });

    it('contains the cancel button for adding name', () => {
      expect(document.getElementById(ADD_NAME_CANCEL_BUTTON)).toBeInTheDocument();
    });

    it('matches the view snapshot', () => {
      expect(document.body).toMatchSnapshot();
    });
  });

  describe('when requireAuth is truthy', () => {
    beforeEach(() => {
      setHTMLFixture(userInfoForm({ requireAuth: true }));
    });

    it('contains the PAT field component', () => {
      expect(document.getElementById(PAT_FIELD_CONTAINER)).toBeInTheDocument();
    });

    it('contains the submit button for adding PAT', () => {
      expect(document.getElementById(ADD_PAT_BUTTON)).toBeInTheDocument();
    });

    it('contains the cancel button for adding PAT', () => {
      expect(document.getElementById(ADD_PAT_CANCEL_BUTTON)).toBeInTheDocument();
    });

    it('matches the view snapshot', () => {
      expect(document.body).toMatchSnapshot();
    });
  });
});
