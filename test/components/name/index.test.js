import name from '~/components/name';
import { setHTMLFixture } from '../../helpers';
import { selectAddNameBox, selectAddEmailBox } from '~/components/utils';

describe('the name input component', () => {
  beforeEach(() => {
    setHTMLFixture(name({}));
  });

  it('contains the name field', () => {
    expect(selectAddNameBox()).toBeInTheDocument();
  });

  it('contains the email field', () => {
    expect(selectAddEmailBox()).toBeInTheDocument();
  });

  it('matches the view snapshot', () => {
    expect(document.body).toMatchSnapshot();
  });
});
