import name from '~/components/name';
import { getSavedName, saveName } from '~/components/name/storage';
import { sessionStorage, STORAGE_NAME, STORAGE_EMAIL } from '~/shared';
import { selectAddNameBox, selectAddEmailBox } from '~/components/utils';
import { createWrapperWithContent } from '../../helpers';

const mockName = 'fake name';
const mockEmail = 'fake@gmail.com';

describe('name storage functions', () => {
  beforeEach(() => {
    createWrapperWithContent(name({}));
  });

  afterEach(() => {
    sessionStorage.removeItem(STORAGE_NAME);
  });

  describe('getSavedName()', () => {
    it('returns the saved name', () => {
      sessionStorage.setItem(STORAGE_NAME, mockName);

      expect(getSavedName()).toBe(mockName);
    });

    it('returns null if name is not saved', () => {
      expect(getSavedName()).toBe('');
    });
  });

  describe('saveName()', () => {
    it('should save name in storage', () => {
      selectAddNameBox().value = mockName;
      saveName({});

      expect(sessionStorage.getItem(STORAGE_NAME)).toBe(mockName);
    });

    it('should save name in state', () => {
      const state = {};
      selectAddNameBox().value = mockName;
      saveName(state);

      expect(state.userName).toBe(mockName);
    });

    it('should save email in storage', () => {
      selectAddEmailBox().value = mockEmail;
      saveName({});

      expect(sessionStorage.getItem(STORAGE_EMAIL)).toBe(mockEmail);
    });

    it('should save email in state', () => {
      const state = {};
      selectAddEmailBox().value = mockEmail;
      saveName(state);

      expect(state.userEmail).toBe(mockEmail);
    });
  });
});
