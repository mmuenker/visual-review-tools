import name from '~/components/name';
import { goToFeedbackForm, isNameValid, isEmailValid } from '~/components/name/helpers';
import { createWrapperWithContent } from '../../helpers';
import { selectCommentBox } from '~/components/utils';

const state = { mergeRequestId: '4815162342' };

describe('name helper functions', () => {
  beforeEach(() => {
    createWrapperWithContent(name({}));
  });

  describe('isNameValid()', () => {
    it('returns false when no name is provided', () => {
      expect(isNameValid({ userName: '' })).toBe(false);
    });

    it('returns true if name is provided', () => {
      expect(isNameValid({ userName: 'fake name' })).toBe(true);
    });
  });

  describe('isEmailValid()', () => {
    it('returns true when no email is provided', () => {
      expect(isEmailValid({ userEmail: '' })).toBe(true);
    });

    it('returns true if valid email is provided', () => {
      expect(isEmailValid({ userEmail: 'fake@gmail.com' })).toBe(true);
    });

    it('returns false if invalid email is provided', () => {
      expect(isEmailValid({ userEmail: 'fake%gmail.com' })).toBe(false);
    });
  });

  describe('goToFeedbackForm()', () => {
    it('moves to the comment view', () => {
      goToFeedbackForm(state);

      expect(selectCommentBox()).toBeInTheDocument();
    });
  });
});
