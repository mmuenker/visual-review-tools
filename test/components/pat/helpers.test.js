import patField from '~/components/pat';
import { isPatValid } from '~/components/pat/helpers';
import { createWrapperWithContent } from '../../helpers';

describe('PAT helper functions', () => {
  beforeEach(() => {
    createWrapperWithContent(patField({}));
  });

  describe('isPatValid()', () => {
    it('returns false when no PAT is provided', () => {
      expect(isPatValid({ personalAccessToken: '' })).toBe(false);
    });

    it('returns true if PAT is provided', () => {
      expect(isPatValid({ personalAccessToken: 'fake-token' })).toBe(true);
    });
  });
});
