import patField from '~/components/pat';
import { getPersonalAccessToken, savePersonalAccessToken } from '~/components/pat/storage';
import { selectAddPatBox } from '~/components/utils';
import { createWrapperWithContent } from '../../helpers';

const mockPat = 'fake-token-123';

describe('PAT storage functions', () => {
  beforeEach(() => {
    createWrapperWithContent(patField({}));
  });

  describe('getPersonalAccessToken()', () => {
    it('returns the saved name', () => {
      const state = { personalAccessToken: mockPat };
      expect(getPersonalAccessToken(state)).toBe(mockPat);
    });

    it('returns empty string if PAT is not saved', () => {
      expect(getPersonalAccessToken({})).toBe('');
    });
  });

  describe('savePersonalAccessToken()', () => {
    it('should save PAT in state', () => {
      const state = {};
      selectAddPatBox().value = mockPat;
      savePersonalAccessToken(state);

      expect(state.personalAccessToken).toBe(mockPat);
    });
  });
});
