import patField from '~/components/pat';
import { setHTMLFixture } from '../../helpers';
import { selectAddPatBox } from '~/components/utils';

describe('the name input component', () => {
  beforeEach(() => {
    setHTMLFixture(patField({}));
  });

  it('contains the PAT field', () => {
    expect(selectAddPatBox()).toBeInTheDocument();
  });

  it('matches the view snapshot', () => {
    expect(document.body).toMatchSnapshot();
  });
});
