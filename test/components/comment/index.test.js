import comment from '~/components/comment';
import { setHTMLFixture } from '../../helpers';
import { CHANGE_MR_ID_BUTTON } from '~/shared';
import { selectCommentBox, selectCommentButton } from '~/components/utils';

describe('the comment input component', () => {
  beforeEach(() => {
    setHTMLFixture(comment({ mergeRequestId: '4815162342' }));
  });

  it('contains the comment field', () => {
    expect(selectCommentBox()).toBeInTheDocument();
  });

  it('contains the submit button', () => {
    expect(selectCommentButton()).toBeInTheDocument();
  });

  it('contains the change merge request button', () => {
    expect(document.getElementById(CHANGE_MR_ID_BUTTON)).toBeInTheDocument();
  });

  it('matches the view snapshot', () => {
    expect(document.body).toMatchSnapshot();
  });
});
