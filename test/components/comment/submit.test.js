import fetchMock from 'fetch-mock-jest';
import { createWrapperWithContent } from '../../helpers';
import comment from '~/components/comment';
import { postComment } from '~/components/comment/submit';

describe('postComment()', () => {
  let state;
  let endpoint;

  beforeEach(() => {
    state = {
      comment: 'some comment',
      platform: 'MacIntel',
      browser: 'Chrome',
      userAgent: 'Mozilla/5.0',
      innerWidth: 1271,
      innerHeight: 830,
      projectId: 31,
      projectPath: 'foo/bar',
      mergeRequestId: 77,
      mrUrl: 'http://example.com',
      userName: 'user1',
    };

    endpoint = `${state.mrUrl}/api/v4/projects/${state.projectId}/merge_requests/${state.mergeRequestId}/visual_review_discussions`;
    fetchMock.post(endpoint, 201);

    createWrapperWithContent(comment(state));
  });

  afterEach(() => {
    fetchMock.reset();
  });

  describe('when personalAccessToken is not present', () => {
    it('makes a request without the Private-Token header', async () => {
      await postComment(state);

      expect(fetchMock).toHaveLastFetched(endpoint, {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
      });
    });
  });

  describe('when personalAccessToken is present', () => {
    beforeEach(() => {
      state.personalAccessToken = 'fake-token';
    });

    it('makes a request with the Private-Token header', async () => {
      await postComment(state);

      expect(fetchMock).toHaveLastFetched(endpoint, {
        headers: {
          'Content-Type': 'application/json',
          'Private-Token': state.personalAccessToken,
        },
        method: 'post',
      });
    });
  });
});
