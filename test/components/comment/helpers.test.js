import comment from '~/components/comment';
import { localStorage, STORAGE_MR_ID } from '~/shared';
import { createWrapperWithContent } from '../../helpers';
import { addNameToComment, changeSelectedMr, clearMrId } from '~/components/comment/helpers';
import { selectAddNameBox, selectMrBox } from '~/components/utils';

const state = {};
const mergeRequestId = '4815162342';

describe('helper functions', () => {
  beforeEach(() => {
    createWrapperWithContent(comment(state));
    state.mergeRequestId = mergeRequestId;
    localStorage.setItem(STORAGE_MR_ID, mergeRequestId);
  });

  describe('clearMrId()', () => {
    beforeEach(() => {
      clearMrId(state);
    });

    it('removes merge request ID from storage', () => {
      expect(localStorage.getItem(STORAGE_MR_ID)).toBe(null);
    });

    it('removes merge request ID from state', () => {
      expect(state.mergeRequestId).toBe('');
    });
  });

  describe('changeSelectedMr()', () => {
    beforeEach(() => {
      localStorage.setItem(STORAGE_MR_ID, mergeRequestId);
      changeSelectedMr(state);
    });

    it('clears the saved merge request ID', () => {
      expect(localStorage.getItem(STORAGE_MR_ID)).toBe(null);
      expect(state.mergeRequestId).toBe('');
    });

    it('moves to the merge request ID view', () => {
      expect(selectMrBox()).toBeInTheDocument();
    });
  });

  describe('addNameToComment()', () => {
    it('moves to the add name view', () => {
      addNameToComment(state);

      expect(selectAddNameBox()).toBeInTheDocument();
    });
  });
});
