import comment from '~/components/comment';
import { sessionStorage, STORAGE_COMMENT, STORAGE_MR_ID } from '~/shared';
import { setHTMLFixture } from '../../helpers';
import { getSavedComment, saveComment, clearSavedComment } from '~/components/comment/storage';
import { selectCommentBox } from '~/components/utils';

const state = {};
const mergeRequestId = '4815162342';
const mockComment = 'testing123';

describe('comment storage functions', () => {
  beforeEach(() => {
    state.mergeRequestId = mergeRequestId;
    localStorage.setItem(STORAGE_MR_ID, mergeRequestId);
    setHTMLFixture(comment(state));
    selectCommentBox().value = mockComment;
  });

  describe('getSavedComment()', () => {
    beforeEach(() => {
      sessionStorage.setItem(STORAGE_COMMENT, mockComment);
    });

    it('should return the saved comment', () => {
      expect(getSavedComment()).toBe(mockComment);
    });
  });

  describe('saveComment()', () => {
    it('should save comment in storage', () => {
      saveComment(state);
      expect(sessionStorage.getItem(STORAGE_COMMENT)).toBe(mockComment);
    });

    it('should save comment in state', () => {
      saveComment(state);
      expect(state.comment).toBe(mockComment);
    });
  });

  describe('clearSavedComment()', () => {
    it('should remove saved comment from storage', () => {
      clearSavedComment();

      expect(sessionStorage.getItem(STORAGE_COMMENT)).toBe(null);
    });
  });
});
